 <!-- title: Best Practice: IGEL Universal Management Suite -->

# IGEL Universal Management Suite (UMS) Server <!-- omit in toc -->

## Table of Contents <!-- omit in toc -->


- [Sizing an IGEL UMS Server](#sizing-an-igel-ums-server)
- [Types of Deployments](#types-of-deployments)
  - [Single UMS Configurations](#single-ums-configurations)
  - [Multi-Server HA UMS Configurations](#multi-server-ha-ums-configurations)
    - [Small UMS Environment (UMS S)](#small-ums-environment-ums-s)
      - [When to Deploy a UMS S Configuration](#when-to-deploy-a-ums-s-configuration)
      - [When to not Deploy a UMS S Configuration](#when-to-not-deploy-a-ums-s-configuration)
- [Medium Sized Environment (UMS M)](#medium-sized-environment-ums-m)
    - [When to Deploy a UMS M Configuration](#when-to-deploy-a-ums-m-configuration)
  - [External File and Update Repository](#external-file-and-update-repository)
- [Reference](#reference)
  - [Maintainers and Contributors](#maintainers-and-contributors)

## Sizing an IGEL UMS Server

Although IGEL provides a general reference via the [IGEL UMS Installation and Sizing Guide KB Article](https://kb.igel.com/endpointmgmt-6.09/en/igel-ums-installation-types-diagrams-53215069.html) and we will use this for all server specifications.there are many factors not taken into account on this article. The numbers for this guide were done under the assumption that the UMS configuration is basic and that you may extend it in ways not mentioned. We will go over these exceptions in the sections below.

## Types of Deployments

UMS Offers multiple deployment configuration options to fit any environment, from the small 20 device shop, to enterprises with over 100,000 devices. If you not familiar with IGEL deployments, we would recommend engaging with a certified IGEL partner, or the [IGEL Advanced Services](https://www.igel.com/igel-solution-family/advanced-services/) team.

### Single UMS Configurations

- [Small w/Embedded Derby Database (UMS S)](#small-ums-environment-ums-s) - Recommended for "Proof of Concept" environments, lab environments, and small/simple environments with less than 5 administrators and less than 1,00 devices
- [Medium w/External Database](#medium-sized-environment-ums-m)

### Multi-Server HA UMS Configurations

- Small and Medium Environments with HA (UMS M/S)

#### Small UMS Environment (UMS S)

Based on the IGEL KB Reference - [Small IGEL Environments](https://kb.igel.com/endpointmgmt-6.10/en/small-environment-ums-s-57320931.html)

![UMS S Table](./.resources/ums/ums-s-table.png)

##### When to Deploy a UMS S Configuration

- Lab environments
- Proof of concept deployments
- Small, simple environments that will not hae more than a few administrators and less than 3,000 devices

##### When to not Deploy a UMS S Configuration 

- At this time, I personally cannot recommend the UMS S configuration for production environments
- You believe that your IGEL deployment will grow to more than 3,000 devices in the future. [^2]
- You have more than 10 IGEL administrators that access the console regularly.
- You will have more than 100 devices which power on within an hour of each other.
  - This can cause a boot storm [^3]
- You plan to upgrade more than 200 IGEL devices at a time
  - You may still utilize a small UMS configuration if you have deployed an [exernal file and update repository](#external-file-and-update-repository) on an external server


## Medium Sized Environment (UMS M)

#### When to Deploy a UMS M Configuration

- You have more than 20 users who may use the IGEL UMS console at the same time
- More than a couple hundred devices power on at the same time
- You are utilizing [UMS Jobs regularly](https://kb.igel.com/endpointmgmt-5.09/en/jobs-22458677.html)
- You are deploying IGEL OS updates to more than 200 devices in a single day
  - For any deployments larger
- You plan to deploy one more IGEL Cloud Gateway (ICG) servers


### External File and Update Repository

## Reference

- IGEL KB - [IGEL UMS Installation and Sizing Guide](https://kb.igel.com/endpointmgmt-6.09/en/igel-ums-installation-types-diagrams-53215069.html)

### Maintainers and Contributors

| Name                 |        Role         |
|:---------------------|:-------------------:|
| Christopher S. Bates | Author / Maintainer |
| Joshua Locher        |     Contributor     |



<!-- Footnotes -->

[^1]: There may be additional circumstances which require migrating to a larger environment that are not listed here. We recommend speaking with your IGEL Partner or the [IGEL Advanced Services](https://www.igel.com/igel-solution-family/advanced-services/) team when designing your first IGEL Universal Management Suite Deployment.

[^2]: The IGEL KB states 5,000 max devices, but again, this is assuming that those devices are fairly static. Trying to actually push changes to more than a few thousand devices from a single UMS server you will most likely trigger an Update storm and be waiting a while for UMS to become responsive again.

[^3]: "Bootstorms" and "Update Storms" These are triggerred when more devices try to request resources or updates from the UMS server / cluster than what can be provided in a timely matter. These storms can cause UMS to become unresponsive as it tries to keep. 