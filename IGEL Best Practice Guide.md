# IGEL Best Practice Guide 

Author: Christopher S. Bates




<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>


## Table of Contents
- [Changelog](./CHANGELOG.md)
- **Sections**
  - **IGEL Softwware**
    - [UMS Server Configuration](./sections/UMS%20and%20ICG.md)
  - **Sessions and Integrations**
    - [Azure Virtal Desktop (AVD)](./sections/Azure%20Virtual%20Desktop)
    - [Citrix](./sections/Citrix.md)
    - [VMware Horizon](./sections/VMWare%20Horizon.md)
- [Maintainers and Contributions](#maintainers-and-contributions)



## Document Statement

This document is not officially santioned by IGEL, but is a community driven project currated and maintained by the Author(s) and Maintainer(s). A

### Project Maintainers and Contributors

| Name                 |        Role         |
|:---------------------|:-------------------:|
| Christopher S. Bates | Maintainer / Author |

| Section                  |                                  Author(s)                                  |
|:-------------------------|:---:|
| Azure Virtual Desktop    |                                                                             |
| Citrix                   |                                                                             |
| Microsoft Remote Desktop |                                                                             |
| VMWare Horizon           | [Christopher S. Bates](mailto:chris.bates@originalme.net), [Joshua B. Locher](mailto:locher@igel.com) |
